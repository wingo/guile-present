;; About the package
(define *name* "Guile Present")
(define *description* "Making presentations with Guile Scheme")
(define *version* "0.3.0")
(define *updated* "21 September 2014")
(define *authors*
  '(("Andy Wingo" . "wingo@pobox.com")))

;; Copying the documentation
(define *copyright-holder* "Andy Wingo")
(define *years* '(2014))
(define *permissions*
  "Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License or (at
your option) any later version.  You should have received a copy of the GNU Lesser General Public
License along with this manual; if not, see http://www.gnu.org/licenses/.")

;; Texinfo info
(define *texinfo-basename* "guile-present")
(define *texinfo-category* "The Algorithmic Language Scheme")
(define *extra-texinfo-menu-entries*
  '(("Concept Index")
    ("Function Index")))
(define *texinfo-epilogue*
  `((node (% (name "Concept Index")))
    (unnumbered "Concept Index")
    (printindex (% (type "cp")))
    (node (% (name "Function Index")))
    (unnumbered "Function Index")
    (printindex (% (type "fn")))))

;; HTML foo
(define *html-relative-root-path* "../../../")
(define *extra-html-entry-files* '())

;; The modules to document
(define *modules*
  '(((present)
     "Main present interface")
    ((present org-mode)
     "Parse org-mode documents into the presentation language")
    ((present texinfo)
     "Parse presentations from Texinfo")
    ((present cairo)
     "Render presentations with the Cairo graphics library")
    ((present svg)
     "Render presentations as SVG graphics")
    ((present fold)
     "The algorithms that underly the SVG layout operation")
    ((present util)
     "Utility functions")))

;; Scripts to document
(define *scripts*
  '(("bin/org-to-pdf-presentation"
     "Make PDF presentations from Org Mode files")
    ("bin/texi-to-pdf-presentation"
     "Make PDF presentations from Texinfo files")))

(define *module-sources* '())
