\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename guile-present.info
@settitle Guile Present
@c %**end of header

@copying 
This manual is for Guile Present (version 0.3.0, updated 21 September
2014)

Copyright 2014 Andy Wingo

@quotation 
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License or (at
your option) any later version.  You should have received a copy of the
GNU Lesser General Public License along with this manual; if not, see
http://www.gnu.org/licenses/.

@end quotation

@end copying

@dircategory The Algorithmic Language Scheme
@direntry 
* Guile Present: (guile-present.info).  Making presentations with Guile Scheme.
@end direntry

@titlepage 
@title Guile Present
@subtitle version 0.3.0, updated 21 September 2014
@author Andy Wingo (@email{wingo@@pobox.com})
@page 
@vskip 0pt plus 1filll
@insertcopying 
@end titlepage

@ifnottex 
@node Top
@top Guile Present
@insertcopying 
@menu 
* present::              Main present interface
* present org-mode::     Parse org-mode documents into the presentation language
* present texinfo::      Parse presentations from Texinfo
* present cairo::        Render presentations with the Cairo graphics library
* present svg::          Render presentations as SVG graphics
* present fold::         The algorithms that underly the SVG layout operation
* present util::         Utility functions

* org-to-pdf-presentation::  Make PDF presentations from Org Mode files
* texi-to-pdf-presentation::  Make PDF presentations from Texinfo files

* Concept Index::        
* Function Index::       
@end menu

@end ifnottex

@iftex 
@shortcontents 
@end iftex

@node present
@chapter (present)
@section Overview
Guile-Present is a library to create presentations in Guile Scheme.  It
has three parts:

@itemize @bullet
@item
A specification of the declarative @dfn{presentation} language;

@item
A set of parsers to transform other formats to the presentation
language; and

@item
A rendering library to render a presentation onto some kind of graphics
device.

@end itemize

For examples of a parser, see @ref{present org-mode,,(present
org-mode)}.

Renderers are built on a pure-functional layout algorithm
(@pxref{present fold,,(present fold)}).

One renderer (@pxref{present svg presentation->svg,,presentation->svg})
produces a single layered SVG.

Guile-Present also includes a command line tool
(@pxref{org-to-pdf-presentation,,org-to-pdf-presentation}) to render an
Org Mode file directly into a PDF suitable for presenting with a PDF
viewer.

@xref{Summary,Org mode,Summary,org,Org Mode Manual}, for more
information on Org Mode.

@section Presentation language
The presentation language defined in Guile-Present is a dialect of SXML.
See @uref{http://ssax.sourceforge.net/} for more information on SXML.

The elements defined in the presentation language are as follows:

@table @code
@item presentation
The top-level node.  No attributes defined; may contain @code{slide}
elements.

@item slide
Defines a slide.  May contain attributes @code{title} and @code{level},
where @code{level} is an integer indicating the level of subsectioning.

@item title
A title, as might appear on a title slide.  May contain text elements,
which will normally be rendered on a separate lines.

@item header
A header line.  May contain text elements, which will normally be
rendered on a separate lines.

@item image
An image.  May not contain subelements.  Will open an image named the
current value of the image-filename parameter, at the size image-width
and image-height.

@item p
A set of text lines.  No attributes defined.  Each child text element
will normally be rendered on a separate line.

@item ul
An unordered list.  May only contain @code{li} elements.  No attributes
defined.

@item li
A list element.  May contain @code{p} or @code{ul} elements.  No
attributes defined.

@end table

For example, the following example shows a presentation with one title
slide and one body slide:

@example 
 (presentation
  (slide (@@ (title "title-slide") (level 0))
         (title "Presentation language"))
  (slide (@@ (title "Summary") (level 1))
         (header "Summary")
         (p "The presentation language"
            "is not particularly expressive.")
         (ul
          (li (p "Few elements defined")
              (ul (li (p "This will change with time"))))
          (li "Unclear specification"))
         (p "However, it is what we have.")))
 
@end example

@section Usage
@node present org-mode
@chapter (present org-mode)
@section Overview
Routines to read files written in Emacs' @code{org-mode}, parsing them
into the presentation SXML language.

@section Usage
@anchor{present org-mode org->presentation}@defun org->presentation port
Parse a file written in Emacs' org-mode into the presentation SXML
format.

Note that only a limited subset of org-mode's syntax is understood.
Specifically, inline text formatters (e.g.  @samp{_emphasized_}) are not
parsed, although it would be nice to do so in the future.

Here is an example of a document that this code understands:

@example 
# -*- mode: org; fill-column: 34 -*-
#+TITLE: Presentation title

* Outline

First we talk about this

Then we talk about that

* This

** This: foo

Whereas
 * Foo is an elegant solution
   to problems in the bar domain
   * Still, baz
 * Zag
 * Metasyntactic

Foo!
@end example

@end defun

@node present texinfo
@chapter (present texinfo)
@section Overview
Transform parsed texinfo into the presentation SXML language.  See
Guile-Lib's @code{(texinfo)} for more information.

@section Usage
@anchor{present texinfo stexi->presentation}@defun stexi->presentation stexi
Transform stexi into the presentation SXML format.

Note that only a limited subset of texinfo is understood.

@end defun

@node present cairo
@chapter (present cairo)
@section Overview
@verbatim 
 Routines to render SXML documents from the presentation vocabulary
 using the Cairo graphics library.
@end verbatim

@section Usage
@anchor{present cairo presentation-render-cairo}@defun presentation-render-cairo presentation cr
Convert an SXML document in the @code{presentation} vocabulary to a
multi-layered SVG.

The result will still be a document in SXML format, so if you want to
write it to disk, use @code{sxml->xml}.  @xref{sxml simple,(sxml
simple),(sxml simple),guile-library,Guile Library}, for more
information.

The resulting SVG will be written with annotations readable by Inkscape,
a vector graphics editor, which help to make the SVG easily editable.  If
your toolchain does not understand namespaces, you might want to filter
out elements that start with @samp{sodipodi:}, @samp{xmlns:}, and
@samp{inkscape:}.

@end defun

@node present svg
@chapter (present svg)
@section Overview
Routines to transform SXML documents from the presentation vocabulary
into SVG.

The code in this file was originally presented in Andy Wingo's 2007
paper, @emph{Applications of fold to XML transformation}.

@section Usage
@anchor{present svg presentation->svg}@defun presentation->svg presentation
Convert an SXML document in the @code{presentation} vocabulary to a
multi-layered SVG.

The result will still be a document in SXML format, so if you want to
write it to disk, use @code{sxml->xml}.  @xref{sxml simple,(sxml
simple),(sxml simple),guile-library,Guile Library}, for more
information.

The resulting SVG will be written with annotations readable by Inkscape,
a vector graphics editor, which help to make the SVG easily editable.  If
your toolchain does not understand namespaces, you might want to filter
out elements that start with @samp{sodipodi:}, @samp{xmlns:}, and
@samp{inkscape:}.

@end defun

@node present fold
@chapter (present fold)
@section Overview
@code{(present fold)} defines a number of variants of the @dfn{fold}
algorithm for use in transforming presentations.  Additionally it
defines the layout operator, @code{fold-layout}, which might be
described as a context-passing variant of SSAX's @code{pre-post-order}.

@section Usage
@anchor{present fold foldt}@defun foldt fup fhere tree
The standard multithreaded tree fold.

@var{fup} is of type [a] -> a.  @var{fhere} is of type object -> a.

@end defun

@anchor{present fold fold}@defun fold proc seed list
The standard list fold.

@var{proc} is of type a -> b -> b.  @var{seed} is of type b.  @var{list}
is of type [a].

@end defun

@anchor{present fold foldts}@defun foldts fdown fup fhere seed tree
The single-threaded tree fold originally defined in SSAX.  @xref{sxml
ssax,,(sxml ssax),guile-library,Guile Library}, for more information.

@end defun

@anchor{present fold foldts*}@defun foldts* fdown fup fhere seed tree
A variant of @ref{present fold foldts,,foldts} that allows pre-order
tree rewrites.  Originally defined in Andy Wingo's 2007 paper,
@emph{Applications of fold to XML transformation}.

@end defun

@anchor{present fold fold-values}@defun fold-values proc list . seeds
A variant of @ref{present fold fold,,fold} that allows multi-valued
seeds.  Note that the order of the arguments differs from that of
@code{fold}.

@end defun

@anchor{present fold foldts*-values}@defun foldts*-values fdown fup fhere tree . seeds
A variant of @ref{present fold foldts*,,foldts*} that allows
multi-valued seeds.  Originally defined in Andy Wingo's 2007 paper,
@emph{Applications of fold to XML transformation}.

@end defun

@anchor{present fold fold-layout}@defun fold-layout tree bindings params layout stylesheet
A traversal combinator in the spirit of SSAX's @ref{sxml transform
pre-post-order,,pre-post-order,guile-library,Guile Library}.

@code{fold-layout} was originally presented in Andy Wingo's 2007 paper,
@emph{Applications of fold to XML transformation}.

@example 
bindings := (<binding>...)
binding  := (<tag> <handler-pair>...)
          | (*default* <handler-pair> ...)
          | (*text* . <text-handler>)
tag      := <symbol>
handler-pair := (pre-layout . <pre-layout-handler>)
          | (post . <post-handler>)
          | (bindings . <bindings>)
          | (pre . <pre-handler>)
          | (macro . <macro-handler>)
@end example

@table @var
@item pre-layout-handler
A function of three arguments:

@table @var
@item kids
the kids of the current node, before traversal

@item params
the params of the current node

@item layout
the layout coming into this node

@end table

@var{pre-layout-handler} is expected to use this information to return a
layout to pass to the kids.  The default implementation returns the
layout given in the arguments.

@item post-handler
A function of five arguments:

@table @var
@item tag
the current tag being processed

@item params
the params of the current node

@item layout
the layout coming into the current node, before any kids were processed

@item klayout
the layout after processing all of the children

@item kids
the already-processed child nodes

@end table

@var{post-handler} should return two values, the layout to pass to the
next node and the final tree.

@item text-handler
@var{text-handler} is a function of three arguments:

@table @var
@item text
the string

@item params
the current params

@item layout
the current layout

@end table

@var{text-handler} should return two values, the layout to pass to the
next node and the value to which the string should transform.

@end table

@end defun

@node present util
@chapter (present util)
@section Overview
@verbatim 
 Utility procedures and macros.
@end verbatim

@section Usage
@anchor{present util match-bind}@defspec match-bind 
Match a string against a regular expression, binding lexical variables
to the various parts of the match.

@var{vars} is a list of names to which to bind the parts of the match.
The first variable of the list will be bound to the entire match, so the
number of variables needed will be equal to the number of open
parentheses (`(') in the pattern, plus one for the whole match.

@var{consequent} is executed if the given expression @var{str} matches
@var{regex}.  If the string does not match, @var{alternate} will be
executed if present.  If @var{alternate} is not present, the result of
@code{match-bind} is unspecified.

Here is a short example:

@example 
 (define (star-indent line)
   "Returns the number of spaces until the first
    star (`*') in the input, or #f if the first
    non-space character is not a star."
   (match-bind "^( *)\*.*$" line (_ spaces)
               (string-length spaces)
               #f))
@end example

@code{match-bind} compiles the regular expression @var{regex} at macro
expansion time.  For this reason, @var{regex} must be a string literal,
not an arbitrary expression.

@end defspec

@node org-to-pdf-presentation
@chapter org-to-pdf-presentation
@section Overview
@code{org-to-pdf-presentation} is a command-line script offered by
Guile-Lib that can transform a file written in Emacs' Org Mode and
directly produce a PDF file, suitable for presenting with a PDF viewer.

@code{org-to-pdf-presentation} works by rendering each slide as an SVG,
then using @code{librsvg} to convert the SVGs to one PDF of many pages.
You will need the tool, @code{rsvg-convert}, provided under Debian as
@code{librsvg2-bin}.

@section Usage
@anchor{deffn-org-to-pdf-presentation}@deffn Command org-to-pdf-presentation in-org-file out-pdf-file
Convert the Org Mode file @var{in-org-file} into a PDF suitable for
presentations.

This command is subject to the limitations of @code{org->presentation}.
Namely, only a subset of all Org Mode constructs are supported.
@xref{present org-mode org->presentation,,org->presentation}, for more
information.

@end deffn

@node texi-to-pdf-presentation
@chapter texi-to-pdf-presentation
@section Overview
@code{texi-to-pdf-presentation} is a command-line script offered by
Guile-Present that can transform a file written in a subset of Texinfo
into a presentation as a PDF file, suitable for presenting with a PDF
viewer.

@code{texi-to-pdf-presentation} works by rendering each slide using
Guile-Cairo.  You will need Guile-Cairo, and optionally guile-rsvg if
you include SVG images.

@section Usage
@anchor{deffn-texi-to-pdf-presentation}@deffn Command texi-to-pdf-presentation in-texi-file out-pdf-file
Convert the Texinfo file @var{in-texi-file} into a PDF suitable for
presentations.

This command is subject to the limitations of
@code{stexi->presentation}.  Namely, only a subset of all Texinfo
constructs are supported.  @xref{present texinfo
stexi->presentation,,stexi->presentation}, for more information.

@end deffn

@node Concept Index
@unnumbered Concept Index
@printindex cp
@node Function Index
@unnumbered Function Index
@printindex fn
@bye
