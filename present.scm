;; guile-present
;; Copyright (C) 2007, 2014 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Guile-Present is a library to create presentations in Guile Scheme.
;; It has three parts:
;;
;; @itemize
;; @item A specification of the declarative @dfn{presentation} language;
;; @item A set of parsers to transform other formats to the presentation
;; language; and
;; @item A rendering library to render a presentation onto some kind of
;; graphics device.
;; @end itemize
;;
;; For examples of a parser, see @ref{present org-mode,,(present
;; org-mode)}.
;;
;; Renderers are built on a pure-functional layout algorithm
;; (@pxref{present fold,,(present fold)}).
;;
;; One renderer (@pxref{present svg
;; presentation->svg,,presentation->svg}) produces a single layered SVG.
;;
;; Guile-Present also includes a command line tool
;; (@pxref{org-to-pdf-presentation,,org-to-pdf-presentation}) to render
;; an Org Mode file directly into a PDF suitable for presenting with a
;; PDF viewer.
;;
;; @xref{Summary,Org mode,Summary,org,Org Mode Manual}, for more
;; information on Org Mode.
;;
;; @section Presentation language
;;
;; The presentation language defined in Guile-Present is a dialect of
;; SXML. See @uref{http://ssax.sourceforge.net/} for more information on
;; SXML.
;;
;; The elements defined in the presentation language are as follows:
;;
;; @table @code
;; @item presentation
;; The top-level node. No attributes defined; may contain @code{slide}
;; elements.
;;
;; @item slide
;; Defines a slide. May contain attributes @code{title} and
;; @code{level}, where @code{level} is an integer indicating the level
;; of subsectioning.
;;
;; @item title
;; A title, as might appear on a title slide. May contain text elements,
;; which will normally be rendered on a separate lines.
;;
;; @item header
;; A header line. May contain text elements, which will normally be
;; rendered on a separate lines.
;;
;; @item image
;; An image. May not contain subelements. Will open an image named the
;; current value of the image-filename parameter, at the size
;; image-width and image-height.
;;
;; @item p
;; A set of text lines. No attributes defined. Each child text element
;; will normally be rendered on a separate line.
;;
;; @item ul
;; An unordered list. May only contain @code{li} elements. No attributes
;; defined.
;;
;; @item li
;; A list element. May contain @code{p} or @code{ul} elements. No
;; attributes defined.
;; @end table
;;
;; For example, the following example shows a presentation with one
;; title slide and one body slide:
;;
;; @example
;; (presentation
;;  (slide (@@ (title "title-slide") (level 0))
;;         (title "Presentation language"))
;;  (slide (@@ (title "Summary") (level 1))
;;         (header "Summary")
;;         (p "The presentation language"
;;            "is not particularly expressive.")
;;         (ul
;;          (li (p "Few elements defined")
;;              (ul (li (p "This will change with time"))))
;;          (li "Unclear specification"))
;;         (p "However, it is what we have.")))
;; @end example
;;
;;
;;; Code:

(define-module (present))

