;; guile-lib
;; Copyright (C) 2007, 2012 Andy Wingo <wingo at pobox dot com>
;;
;; The author places this file under the public domain.

;;; Commentary:
;;
;; @code{(present fold)} defines a number of variants of the @dfn{fold}
;; algorithm for use in transforming presentations. Additionally it
;; defines the layout operator, @code{fold-layout}, which might be
;; described as a context-passing variant of SSAX's
;; @code{pre-post-order}.
;;
;;; Code:

(define-module (present fold)
  #:export (foldt
            fold
            foldts
            foldts*
            fold-values
            foldts*-values
            fold-layout))

(define (atom? x)
  (not (pair? x)))

(define (foldt fup fhere tree)
  "The standard multithreaded tree fold.

@var{fup} is of type [a] -> a. @var{fhere} is of type object -> a.
"
  (if (atom? tree)
      (fhere tree)
      (fup (map (lambda (kid)
                  (foldt fup fhere kid))
                tree))))

(define (fold proc seed list)
  "The standard list fold.

@var{proc} is of type a -> b -> b. @var{seed} is of type b. @var{list}
is of type [a]."
  (if (null? list)
      seed
      (fold proc (proc (car list) seed) (cdr list))))

(define (foldts fdown fup fhere seed tree)
  "The single-threaded tree fold originally defined in SSAX.
@xref{sxml ssax,,(sxml ssax),guile-library,Guile Library}, for more
information."
  (if (atom? tree)
      (fhere seed tree)
      (fup seed
           (fold (lambda (kid kseed)
                  (foldts fdown fup fhere kseed kid))
                 (fdown seed tree)
                 tree)
           tree)))

(define (foldts* fdown fup fhere seed tree)
  "A variant of @ref{present fold foldts,,foldts} that allows pre-order
tree rewrites. Originally defined in Andy Wingo's 2007 paper,
@emph{Applications of fold to XML transformation}."
  (if (atom? tree)
      (fhere seed tree)
      (call-with-values
          (lambda () (fdown seed tree))
        (lambda (kseed tree)
          (fup seed
               (fold (lambda (kid kseed)
                       (foldts* fdown fup fhere
                                kseed kid))
                     kseed
                     tree)
               tree)))))

(define (fold-values proc list . seeds)
  "A variant of @ref{present fold fold,,fold} that
allows multi-valued seeds. Note that the order of the arguments differs
from that of @code{fold}."
  (if (null? list)
      (apply values seeds)
      (call-with-values
          (lambda () (apply proc (car list) seeds))
        (lambda seeds
          (apply fold-values proc (cdr list) seeds)))))

(define (foldts*-values fdown fup fhere tree . seeds)
  "A variant of @ref{present fold foldts*,,foldts*} that allows multi-valued seeds.
Originally defined in Andy Wingo's 2007 paper, @emph{Applications of
fold to XML transformation}."
  (if (atom? tree)
      (apply fhere tree seeds)
      (call-with-values
          (lambda () (apply fdown tree seeds))
        (lambda (tree . kseeds)
          (call-with-values
              (lambda ()
                (apply fold-values
                       (lambda (tree . seeds)
                         (apply foldts*-values
                                fdown fup fhere tree seeds))
                       tree kseeds))
            (lambda kseeds
              (apply fup tree (append seeds kseeds))))))))

(define (assq-ref alist key default)
  (cond ((assq key alist) => cdr)
        (else default)))

(define (fold-layout tree bindings params layout stylesheet)
  "A traversal combinator in the spirit of SSAX's @ref{sxml transform
pre-post-order,,pre-post-order,guile-library,Guile Library}.

@code{fold-layout} was originally presented in Andy Wingo's 2007 paper,
@emph{Applications of fold to XML transformation}.

@example
bindings := (<binding>...)
binding  := (<tag> <handler-pair>...)
          | (*default* <handler-pair> ...)
          | (*text* . <text-handler>)
tag      := <symbol>
handler-pair := (pre-layout . <pre-layout-handler>)
          | (post . <post-handler>)
          | (bindings . <bindings>)
          | (pre . <pre-handler>)
          | (macro . <macro-handler>)
@end example

@table @var
@item pre-layout-handler
A function of three arguments:

@table @var
@item kids
the kids of the current node, before traversal
@item params
the params of the current node
@item layout
the layout coming into this node
@end table

@var{pre-layout-handler} is expected to use this information to return a
layout to pass to the kids. The default implementation returns the
layout given in the arguments.

@item post-handler
A function of five arguments:
@table @var
@item tag
the current tag being processed
@item params
the params of the current node
@item layout
the layout coming into the current node, before any kids were processed
@item klayout
the layout after processing all of the children
@item kids
the already-processed child nodes
@end table

@var{post-handler} should return two values, the layout to pass to the
next node and the final tree.

@item text-handler
@var{text-handler} is a function of three arguments:
@table @var
@item text
the string
@item params
the current params
@item layout
the current layout
@end table

@var{text-handler} should return two values, the layout to pass to the
next node and the value to which the string should transform.
@end table
"
  (define (err . args)
    (error "no binding available" args))
  (define *default*
    `((pre-layout . ,(lambda (tree params layout)
                       layout))
      (post . ,(lambda (tag params layout klayout kids)
                 (error "No post binding available for tag" tag)))))
  (define (fdown tree bindings pcont params layout ret)
    (define (macro macro-handler)
      (fdown (apply macro-handler tree)
             bindings pcont params layout ret))
    (define (pre pre-handler)
      (values '() bindings
              (lambda (params layout old-layout kids)
                (values layout (reverse kids)))
              params layout (apply pre-handler tree)))
    (define (fdown-helper new-bindings pre-layout cont)
      (let* ((local-params (and (pair? (cdr tree))
                                (pair? (cadr tree)) (eq? (caadr tree) '@)
                                (cdadr tree)))
             (style-params (assq-ref stylesheet (car tree) '()))
             (params (cons (if local-params
                               (append local-params style-params)
                               style-params)
                           params)))
        (values (if local-params (cddr tree) (cdr tree))
                (if new-bindings (append new-bindings bindings) bindings)
                (lambda args
                  (apply cont (car tree) args))
                params
                (pre-layout tree params layout)
                '())))
    (let ((tag-bindings (assq-ref bindings (car tree) #f))
          (defaults (assq-ref bindings '*default* #f)))
      (define (bindings-ref binding)
        (or (and tag-bindings (assq-ref tag-bindings binding #f))
            (and defaults (assq-ref defaults binding #f))
            (assq-ref *default* binding #f)))
      (cond
       ((bindings-ref 'macro) => macro)
       ((bindings-ref 'pre) => macro)
       (else
        (fdown-helper
         (bindings-ref 'bindings)
         (bindings-ref 'pre-layout)
         (bindings-ref 'post))))))
  (define (fup tree bindings cont params layout ret
               kbindings kcont kparams klayout kret)
    (call-with-values
        (lambda ()
          (kcont kparams layout klayout (reverse kret)))
      (lambda (klayout kret)
        (values bindings cont params klayout (cons kret ret)))))
  (define (fhere tree bindings cont params layout ret)
    (call-with-values
        (lambda ()
          ((assq-ref bindings '*text* err) tree params layout))
      (lambda (tlayout tret)
        (values bindings cont params tlayout (cons tret ret)))))
  (call-with-values
      (lambda ()
        (foldts*-values
         fdown fup fhere tree bindings #f (cons params '()) layout '()))
    (lambda (bindings cont params layout ret)
      (values (car ret) layout))))
