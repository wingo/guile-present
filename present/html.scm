;; guile-present
;; Copyright (C) 2007, 2009, 2010, 2011, 2012, 2014, 2020, 2022 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Routines to render SXML documents from the presentation vocabulary
;; using the Cairo graphics library.
;;
;;; Code:

(define-module (present html)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (web uri)
  #:export (presentation->shtml shtml->html))

(define (transform bindings params x)
  (match x
    ((tag . body)
     (match (assq-ref bindings tag)
       (#f (error "unexpected tagged expr" bindings x))
       (trans
        (match body
          ((('@ . params*) . body)
           (trans (cons params* params) body))
          (_
           (trans params body))))))
    ((? string?)
     (match (assq-ref bindings '*text*)
       (#f (error "unexpected text" x))
       (trans (trans params x))))))

(define (transform* bindings params body)
  (map (lambda (x) (transform bindings params x)) body))

(define* (lookup params key
                 #:optional (default-thunk
                              (lambda ()
                                (error "unbound param" key))))
  (cond ((null? params) (default-thunk))
        ((assq key (car params)) => cadr)
        (else (lookup (cdr params) key default-thunk))))

(define-syntax let-params
  (syntax-rules ()
    ((_ (param-exp ...) (k ...) b b* ...)
     (let ((p (param-exp ...)))
       (let-params p (k ...) b b* ...)))
    ((_ params () b b* ...)
     (begin b b* ...))
    ((_ params ((k default) k* ...) b b* ...)
     (let ((k (lookup params 'k (lambda () default))))
       (let-params params (k* ...) b b* ...)))
    ((_ params (k k* ...) b b* ...)
     (let ((k (lookup params 'k)))
       (let-params params (k* ...) b b* ...)))))

(define (specialize-param params k v)
  (cons (list (list k v)) params))

(define *punctuation*
  '(("``" . "“")
    ("''" . "”")
    ("`" . "‘")
    ("'" . "’")
    ("---" . "—")
    ("--" . "–")))

(define (prettify-punctuation word)
  (let lp ((word word) (punctuation *punctuation*))
    (match punctuation
      (() word)
      (((needle . replacement) . punctuation)
       (cond
        ((string-contains word needle)
         => (lambda (pos)
              (lp (string-append
                   (substring word 0 pos)
                   replacement
                   (substring word (+ pos (string-length needle))))
                  punctuation)))
        (else (lp word punctuation)))))))

(define *presentation*
  `((presentation
     . ,(lambda (params body)
          `(html
            (body
             ,@(transform* *slide* params body)))))))

(define (render-uri base tail)
  (and tail
       (uri->string
        (string->uri
         (if base
             (string-append base tail)
             (begin
               (warn "no URI base for image" tail)
               tail))))))

(define css-renderers (make-hash-table))
(define-syntax-rule (define-css-renderer (tag v) . body)
  (hashq-set! css-renderers 'tag (lambda (v) . body)))
(define-syntax-rule (define-css-renderer-aliases tag other ...)
  (begin
    (hashq-set! css-renderers 'other (hashq-ref css-renderers 'tag))
    ...))

(define-css-renderer (background-color v)
  (and v (format #f "#~6,'0x" v)))
(define-css-renderer (background-image v)
  (match v
    ((_ . #f) #f)
    ((#f . file) (warn "no URI base for image" file) #f)
    ((base . file) (string-append "url(" (render-uri base file) ")"))))
(define-css-renderer (length v)
  (match v
    ((scale . px)
     (format #f "~apx" (inexact->exact (round (* (or scale 1) px)))))))
(define-css-renderer (line-height scale)
  (format #f "~,2f" scale))
(define-css-renderer-aliases length
  width height min-height
  margin-left margin-top margin-right
  padding-left padding-top padding-right
  left top font-size)
(define-css-renderer (margin-bottom v)
  (match v
    ((v . 'em)
     (format #f "~,2fem" v))))

(define (attach-css element . css)
  (define (build-css css)
    (match css
      (() "")
      ((k v . css)
       (let ((render (hashq-ref css-renderers k
                                (lambda (v)
                                  (format #f "~a" v)))))
         (let ((v (render v)))
           (if v
               (format #f "~a:~a;~a" k v (build-css css))
               (build-css css)))))))
  (let ((css (build-css css)))
    (if (string-null? css)
        element
        (match element
          ((tag ('@ . attrs) . body)
           `(,tag (@ (style ,css) . ,attrs) . ,body))
          ((tag . body)
           `(,tag (@ (style ,css)) . ,body))))))

(define *slide*
  `((slide
     . ,(lambda (params body)
          (let-params params (margin-left margin-top margin-right
                              background background-color foreground-color
                              background-size
                              width height text-height text-scaling
                              image-uri-base html-scale
                              line-spacing
                              font-family)
            ;; fixme: bg color, fg color, bg
            (attach-css
             `(div ,@(transform* *block* params body))
             'background-color background-color
             'background-image (cons image-uri-base background)
             'background-size background-size
             'background-repeat "no-repeat"
             'background-position "center"
             'overflow "auto"
             'position "relative"
             'font-family font-family
             'width (cons html-scale (- width margin-left margin-right))
             'min-height (cons html-scale (- height margin-top))
             'padding-left (cons html-scale margin-left)
             'padding-top (cons html-scale margin-top)
             'padding-right (cons html-scale margin-right)
             'line-height line-spacing
             'font-size (cons html-scale (* text-height text-scaling))))))))

(define *block*
  `((title
     . ,(lambda (params body)
          `(h1 ,@(transform* *inline* params body))))
    (header
     . ,(lambda (params body)
          (let-params params (absolute? margin-top margin-left margin-right
                              width html-scale text-height text-scaling)
            (attach-css
             `(h2 ,@(transform* *inline* params body))
             'position (if absolute? "absolute" "relative")
             'left (cons html-scale margin-left)
             'top (cons html-scale margin-top)
             'width (cons html-scale (- width margin-right margin-left))
             'font-size (cons html-scale (* text-height text-scaling))))))
    (image
     . ,(lambda (params body)
          (let-params params (image-filename (image-width #f) (image-height #f)
                              height width margin-right margin-bottom inline?
                              text-height text-scaling line-spacing
                              image-uri-base html-scale block-spacing)
            (match body
              (()
               (attach-css
                `(img (@ (src ,(render-uri image-uri-base image-filename))))
                'display "block"
                'max-width "100%"
                'margin-bottom `(,(1- block-spacing) . em)))))))
    (ul
     . ,(lambda (params body)
          (let-params params (block-spacing)
            (attach-css
             `(ul . ,(match (transform* *ul* params body)
                       ((('li ('p . text)) ...)
                        ;; If all list items have a single para, it's a
                        ;; tight list; remove the paragraphs.
                        (map (lambda (text) `(li . ,text)) text))
                       (items items)))
             'margin-bottom `(,(1- block-spacing) . em)))))
    (elided
     . ,(lambda _ ""))
    (pre
     . ,(lambda (params body)
          (let-params params (html-scale text-height text-scaling
                              block-spacing)
            (attach-css
             `(pre ,@(transform* *inline* params body))
             'font-size (cons html-scale (* text-height text-scaling))
             'margin-bottom `(,(1- block-spacing) . em)))))
    (p
     . ,(lambda (params body)
          (let-params params (block-spacing)
            (attach-css
             `(p ,@(transform* *inline* params body))
             'margin-bottom `(,(1- block-spacing) . em)))))))

(define *ul*
  `((li
     . ,(lambda (params body)
          `(li ,@(transform* *block* params body))))))

(define *inline*
  `((tt
     . ,(lambda (params body)
          `(tt ,@(transform* *inline* params body))))
    (i
     . ,(lambda (params body)
          `(i ,@(transform* *inline* params body))))
    (b
     . ,(lambda (params body)
          `(b ,@(transform* *inline* params body))))
    (span
     . ,(lambda (params body)
          `(span ,@(transform* *inline* params body))))
    (image
     . ,(lambda (params body)
          `(tt ,@(transform* *inline* params body))))
    (*text*
     . ,(lambda (params text)
          (let-params params (pretty-punctuation?)
            (if pretty-punctuation?
                (prettify-punctuation text)
                text))))))

(define *default-params*
  '((indent-width 64)
    (margin-left 64)
    (margin-right 64)
    (margin-top 64)
    (margin-bottom 64)
    (line-spacing 1.1)
    (inline? #f)
    (absolute? #f)
    (can-end-sentence? #t)
    (verbatim? #f)
    (pretty-punctuation? #t)
    (block-spacing 1.4)
    (font-family "Serif")
    (font-slant normal)
    (font-weight normal)
    (bullet-string "❧")
    (bullet-font-family "Sans")
    (text-height 42)
    (text-scaling 1/1)
    (background #f)
    (background-color #xFFFFFF)
    (foreground-color #x000000)
    (width 1024)
    (height 768)
    (image-uri-base #f)))

(define *presentation-stylesheet*
  '((title (text-height 96))
    (header (text-height 64))))

(define* (apply-inline-stylesheet presentation
                                  #:optional (default-styles
                                               *presentation-stylesheet*))
  (define (compute-params tag params styles)
    (match (assq-ref styles tag)
      (#f (if (null? params) '() `((@ . ,params))))
      (params* `((@ . ,(append params params*))))))
  (define (apply-stylesheet elt styles)
    (define (visit-body body)
      (map (lambda (elt) (apply-stylesheet elt styles)) body))
    (match elt
      ((tag ('@ . params) . body)
       `(,tag ,@(compute-params tag params styles) . ,(visit-body body)))
      ((tag . body)
       `(,tag ,@(compute-params tag '() styles) . ,(visit-body body)))
      (_ elt)))
  (define (adjoin tag key value stylesheet)
    (match stylesheet
      (() `((,tag (,key ,value))))
      (((t . params) . stylesheet)
       (if (eq? t tag)
           `((,t (,key ,value) . ,params) . ,stylesheet)
           (acons t params (adjoin tag key value stylesheet))))))
  (match presentation
    (('presentation ('@ . params) . body)
     (let lp ((in params) (out '()) (stylesheet default-styles))
       (match in
         (() (apply-stylesheet `(presentation (@ ,@(reverse out)) . ,body)
                               stylesheet))
         (((and param (tag key value)) . in)
          (lp in out (adjoin tag key value stylesheet)))
         ((param . in)
          (lp in (cons param out) stylesheet)))))
    (_ presentation)))

(define (presentation->shtml presentation)
  (transform *presentation* (list *default-params*)
             (apply-inline-stylesheet presentation)))

(define-syntax-rule (define-tag-set pred tag ...)
  (define pred
    (let ((set (make-hash-table)))
      (hashq-set! set 'tag #t) ...
      (lambda (t) (hashq-ref set t)))))

(define-tag-set void-element?
  area base br col embed hr img input link meta param source track wbr)
(define-tag-set template-element?
  template)
(define-tag-set raw-text-element?
  script style)
(define-tag-set escapable-raw-text-element?
  textarea title)
(define (foreign-element? tag)
  (string-index (symbol->string tag) #\:))
;; Otherwise it's a normal element.

(define (make-char-quotator char-encoding)
  (let ((bad-chars (list->char-set (map car char-encoding))))

    ;; Check to see if str contains one of the characters in charset,
    ;; from the position i onward. If so, return that character's index.
    ;; otherwise, return #f
    (define (index-cset str i charset)
      (string-index str charset i))

    ;; The body of the function
    (lambda (str port)
      (let ((bad-pos (index-cset str 0 bad-chars)))
        (if (not bad-pos)
            (display str port)          ; str had all good chars
            (let loop ((from 0) (to bad-pos))
              (cond
               ((>= from (string-length str)) *unspecified*)
               ((not to)
                (display (substring str from (string-length str)) port))
               (else
                (let ((quoted-char
                       (cdr (assv (string-ref str to) char-encoding)))
                      (new-to
                       (index-cset str (+ 1 to) bad-chars)))
                  (if (< from to)
                      (display (substring str from to) port))
                  (display quoted-char port)
                  (loop (1+ to) new-to))))))))))

(define (attribute-value-empty? value)
  (string-null? value))

(define attribute-value-needs-quotes-chars
  (char-set-union (string->char-set "\"'=<>`") char-set:whitespace))
(define (attribute-value-needs-quotes? value)
  (or (string-null? value)
      (string-index value attribute-value-needs-quotes-chars)))

(define print-attribute-value/quoted
  (make-char-quotator
   '((#\< . "&lt;") (#\> . "&gt;") (#\& . "&amp;") (#\" . "&quot;"))))

(define print-text/quoted
  (make-char-quotator
   '((#\< . "&lt;") (#\> . "&gt;") (#\& . "&amp;"))))

(define* (shtml->html tree #:optional (port (current-output-port))
                      #:key (pretty? #f) (xhtml? #f))
  "Serialize the shtml tree @var{tree} as HTML. The output will be written
to the current output port, unless the optional argument @var{port} is
present."
  (define (whitespace? str)
    (and (string? str)
         (string-every char-set:whitespace str)))
  (define (attribute->html attr value)
    (display attr port)
    (unless (and (not xhtml?) (attribute-value-empty? value))
      (display #\= port)
      (cond
       ((or xhtml? (attribute-value-needs-quotes? value))
        (display #\" port)
        (print-attribute-value/quoted value port)
        (display #\" port))
       (else
        (display value port)))))

  ;; We don't support HTML comments.
  (define (comment? x) #f)

  (define (elide-start-tag? tag attrs body)
    (define child (and (pair? body) (car body)))
    (define empty? (not child))
    (define (element? x) (pair? x))
    (and
     (not xhtml?)
     (null? attrs)
     (case tag
       ((html) (not (comment? child)))
       ((head) (or empty? (element? child)))
       ((body) (match child
                 ((? whitespace?) #f)
                 (((or 'meta 'link 'script 'style 'template) . _) #f)
                 (_ #t)))
       ;; FIXME: Cases for colgroup and tbody depend on previous.
       (else #f))))

  (define (elide-end-tag? tag parent next)
    (define last? (not next))
    (and
     (not xhtml?)
     (case tag
       ((html) (not (comment? next)))
       ((head) (and (not (comment? next))
                    (not (whitespace? next))))
       ((body) (not (comment? next)))
       ((li) (match next
               (('li . _) #t)
               (_ last?)))
       ((dt) (match next
               (((or 'dt 'dd) . _) #t)
               (_ #f)))
       ((dd) (match next
               (((or 'dt 'dd) . _) #t)
               (_ last?)))
       ((p) (match next
              (((or 'address 'article 'aside 'blockquote
                    'details 'div 'dl 'fieldset 'figcaption
                    'figure 'footer 'form 'h1 'h2 'h3 'h4 'h5 'h6
                    'header 'hgroup 'hr 'main 'menu 'nav 'ol 'p
                    'pre 'section 'table 'ul) . _) #t)
              (_ (and last?
                      (match parent
                        ((or 'a 'audio 'del 'ins 'map 'noscript 'video) #f)
                        (_ #t))))))
       ((rt rp) (match next
                  (((or 'rt 'rp) . _) #t)
                  (_ last?)))
       ((optgroup) (match next
                     (('optgroup . _) #t)
                     (_ last?)))
       ((option) (match next
                   (((or 'optgroup 'option) . _) #t)
                   (_ last?)))
       ((colgroup) (and (not (comment? next)) (not (whitespace? next))))
       ((caption) (and (not (comment? next)) (not (whitespace? next))))
       ((thead) (match next
                  (((or 'tbody 'tfoot) . _) #t)
                  (_ #f)))
       ((tbody) (match next
                  (((or 'tbody 'tfoot) . _) #t)
                  (_ last?)))
       ((tfoot) last?)
       ((tr) (match next
               (('tr . _) #t)
               (_ last?)))
       ((td th) (match next
                  (((or 'td 'th) . _) #t)
                  (_ last?)))
       (else #f))))

  (define (element->html tag attrs body parent next)
    (define elide-start? (elide-start-tag? tag attrs body))
    (define (block-tag? tag)
      (match tag
        ((or 'address 'blockquote 'center 'dialog 'div
             'figure 'figcaption 'footer 'form 'header 'hr
             'legend 'listing 'main 'p 'plaintext 'pre 'xml
             'html 'block
             'article 'aside 'h1 'h2 'h3 'h4 'h5 'h6 'hgroup 'nav 'section
             'dir 'dd 'dl 'dt 'menu 'ol 'ul 'li)
         #t)
        (_ #f)))

    (unless elide-start?
      (display #\< port)
      (display tag port)
      (let lp ((attrs attrs))
        (match attrs
          (() #t)
          ((((? symbol? attr) val) . attrs)
           (display #\space port)
           (attribute->html attr val)
           (lp attrs)))))

    (cond
     ((and (null? body) (foreign-element? tag))
      (display " />" port))
     ((and (not xhtml?) (void-element? tag))
      (unless (null? body) (error "unexpected body for void element"))
      (display #\> port))
     (else
      (let ()
        (unless elide-start?
          (display #\> port))
        (define elide-end? (elide-end-tag? tag parent next))
        (define pretty-block?
          (and pretty? (not elide-start?) (not elide-end?)
               (not (null? body)) (block-tag? tag)
               (not (and-map string? body))))
        (when pretty-block? (newline port))
        (cond
         ((raw-text-element? tag)
          (let ((body (string-concatenate body)))
            (let ((needle (string-append "</" (symbol->string tag))))
              (let lp ((idx 0))
                (let ((idx (string-contains-ci body needle idx)))
                  (when idx
                    (let ((idx (+ idx (string-length needle))))
                      (let ((ch (and (< idx (string-length body))
                                     (string-ref body idx))))
                        (when (and ch (string-index "\t\n\f\r >/" ch))
                          (error "raw text element body contains end tag"
                                 needle body)))
                      (lp idx))))))
            (display body port)))
         ((escapable-raw-text-element? tag)
          (for-each
           (lambda (str)
             (unless (string? str)
               (error "bad escapable raw text content" str))
             (print-text/quoted str port))
           body))
         (else
          (let lp ((body body))
            (match body
              (() #f)
              ((tree . body)
               (->html tree tag (match body (() #f) ((next . _) next)))
               (lp body))))))
        (unless elide-end?
          (when pretty-block? (newline port))
          (display "</" port)
          (display tag port)
          (display ">" port)))))

    (when pretty?
      (match next
        (((? block-tag?) . _)
         (newline port))
        (_ #f))))

  (define (->html tree parent next)
    (match tree
      (((? symbol? tag) ('@ . attrs) . body)
       (element->html tag attrs body parent next))
      (((? symbol? tag) . body)
       (element->html tag '() body parent next))
      ((_ . _)
       (error "nodelists unsupported" tree))
      ((or #f #t ()) #f)
      ((? string?)
       (print-text/quoted tree port))
      ((? procedure?)
       (with-output-to-port port tree))
      ((? number?)
       (display tree port))
      (tree
       (error "unexpected shtml" tree))))

  (match tree
    (('html . _)
     (->html tree #f #f))))
