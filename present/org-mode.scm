;; guile-present
;; Copyright (C) 2007, 2010, 2014 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Routines to read files written in Emacs' @code{org-mode}, parsing
;; them into the presentation SXML language.
;;
;;; Code:

(define-module (present org-mode)
  :use-module (present util)
  :use-module (ice-9 rdelim)
  :use-module ((srfi srfi-13) :select (string-trim-both))
  :export (org->presentation))

(define (org->presentation port)
  "Parse a file written in Emacs' org-mode into the presentation SXML
format.

Note that only a limited subset of org-mode's syntax is understood.
Specifically, inline text formatters (e.g. @samp{_emphasized_}) are not
parsed, although it would be nice to do so in the future.

Here is an example of a document that this code understands:

@example
# -*- mode: org; fill-column: 34 -*-
#+TITLE: Presentation title

* Outline

First we talk about this

Then we talk about that

* This

** This: foo

Whereas
 * Foo is an elegant solution
   to problems in the bar domain
   * Still, baz
 * Zag
 * Metasyntactic

Foo!
@end example
"
  (let lp ((line (read-line port))
           (out '())
           (params '()))
    (cond
     ((eof-object? line)
      (if (assq 'TITLE params)
          `(presentation
            (@ (org-params ,@params))
            (slide (@ (title "TITLE") (level 0))
                   (title ,@(string-split (assq-ref params 'TITLE) #\space)))
            ,@(reverse out))
          `(presentation
            ,@(reverse out))))
     ((star-indent line)
      (read-slide line port
                  (lambda (line slide)
                    (lp line (cons slide out) params))))
     (else
      (match-bind "^#\\+([A-Za-z-]+):(.*)$" line (_ k v)
                  (lp (read-line port)
                      out
                      (acons (string->symbol k) (string-trim-both v)
                             params))
                  (lp (read-line port) out params))))))

(define (line-indent line)
  (match-bind "^( *)[^ ]?.*$" line (_ spaces)
              (string-length spaces)
              (error "what!" line)))

(define (star-indent line)
  (match-bind "^( *)\\*.*$" line (_ spaces)
              (string-length spaces)
              #f))

(define (read-paragraph line port indent cont)
  (define (in-paragraph? line)
    (match-bind "^( *)([^ *].*)$" line (_ spaces text)
                (if (>= (string-length spaces) indent)
                    text
                    #f)
                #f))
  (let lp ((line line) (out '(p)) (eating-whitespace? #f))
    (cond
     ((eof-object? line)
      (cont line (reverse out)))
     ((string-null? (string-trim-both line))
      (lp (read-line port) out #t))
     (eating-whitespace?
      (cont line (reverse out)))
     ((in-paragraph? line)
      => (lambda (text)
           (lp (read-line port) (cons text out) #f)))
     (else
      (cont line (reverse out))))))

(define (read-ul line port indent cont)
  (define (read-ul-rest line first)
    (let lp ((line line) (out (cons first '(ul))))
      (cond
       ((star-indent line)
        => (lambda (level)
             (cond
              ((= level indent)
               (read-li line port level
                        (lambda (line li)
                          (lp line (cons li out)))))
              (else
               (cont line (reverse out))))))
       (else
        (cont line (reverse out))))))

  (read-li line port indent
           read-ul-rest))

(define (read-li line port indent cont)
  (define (read-li-rest line first)
    (let lp ((line line) (out (cons first '(li))))
      (cond
       ((star-indent line)
        => (lambda (level)
             (cond
              ((> level indent)
               (read-ul line port level
                        (lambda (line ul)
                          (lp line (cons ul out)))))
              (else
               (cont line (reverse out))))))
       (else
        (if (< (line-indent line) (+ indent 2))
            (cont line (reverse out))
            (read-paragraph line port (+ indent 2)
                            (lambda (line para)
                              (lp line (cons para out)))))))))

  (let ((line (string-append (make-string (1+ indent) #\space)
                             (substring line (1+ indent)))))
    (read-paragraph line port (+ indent 2)
                    read-li-rest)))

(define (read-slide line port cont)
  (define (read-body level title)
    (let lp ((line (read-line port))
             (out `((header ,title) (@ (title ,title) (level ,level)) slide)))
      (cond
       ((eof-object? line)
        (cont line (reverse out)))
       ((string-null? (string-trim-both line))
        (lp (read-line port) out))
       ((star-indent line)
        => (lambda (level)
             (cond
              ((zero? level)
               (cont line (reverse out)))
              (else
               (read-ul line port level
                        (lambda (line ul)
                          (lp line (cons ul out))))))))
       (else
        (read-paragraph line port 0
                        (lambda (line para)
                          (lp line (cons para out))))))))
  (match-bind "^(\\*+) *(.*)$" line (_ stars title)
              (read-body (string-length stars) title)
              (error "what" line)))
