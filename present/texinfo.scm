;; guile-present
;; Copyright (C) 2007, 2011, 2012, 2014, 2021 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Transform parsed texinfo into the presentation SXML language. See
;; Guile-Lib's @code{(texinfo)} for more information.
;;
;;; Code:

(define-module (present texinfo)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:select (append-map drop-while filter-map))
  #:use-module (srfi srfi-26)
  #:use-module (present util)
  #:use-module (present fold)
  #:use-module (string wrap)
  #:use-module ((sxml transform) #:select (pre-post-order))
  #:use-module (container nodal-tree)
  #:use-module (texinfo)
  #:use-module (texinfo nodal-tree)
  #:export (stexi->presentation))

(define (node-foldts fdown fup fhere seed node)
  (let ((seed (fhere seed node)))
    (fup seed
         (fold (lambda (kid kseed)
                 (node-foldts fdown fup fhere kseed kid))
               (fdown seed node)
               (node-children node))
         node)))

(define (make-seed depth out)
  (list depth out))

(define-syntax with-seed
  (syntax-rules ()
    ((_ seed (var ...) expr)
     (apply (lambda (var ...) expr) seed))))

(define (apply-params head locals params)
  (let lp ((in params) (ours '()) (kids '()))
    (match in
      (() (values (append locals ours) kids))
      (((and binding (k v)) . in)
       (lp in (cons binding ours) kids))
      ((((? (cut eq? <> head)) . (and binding (_ _))) . in)
       (lp in (cons binding ours) kids))
      ((((? (cut eq? <> head)) . (and binding (_ _ . _))) . in)
       (lp in ours (cons binding kids)))
      ((binding . in)
       (lp in ours (cons binding kids))))))

(define (decorate-element head locals body params)
  (call-with-values (lambda ()
                      (apply-params head locals params))
    (lambda (locals params)
      (let ((body (map (cut decorate <> params) body)))
        (if (null? locals)
            `(,head ,@body)
            `(,head (@ ,@locals) ,@body))))))

(define (decorate tree params)
  (match tree
    ((head ('@ . locals) . body)
     (decorate-element head locals body params))
    ((head . body)
     (decorate-element head '() body params))
    (_ tree)))

(define *elided* (list 'elided))

(define (canonicalize tree)
  (reverse
   (foldts
    (lambda (seed node)
      '())
    (lambda (seed kseed node)
      (let ((kseed (reverse kseed)))
        (cond
         ((null? kseed) seed)
         ((and (pair? kseed) (not (symbol? (car kseed))))
          (append (reverse kseed) seed))
         (else
          (cons kseed seed)))))
    (lambda (seed node)
      (if (eq? node *elided*)
          seed
          (cons node seed)))
    '()
    tree)))

(define (stexi->presentation stexi)
  "Transform stexi into the presentation SXML format.

Note that only a limited subset of texinfo is understood.
"
  (define (parse-param form)
    (match form
      (('c ('% ('all str)))
       (match-bind "^ *\\+([A-Za-z-][A-Za-z -]*[?]?):(.*)$" str (_ k v)
         (append (map string->symbol
                      (filter (negate string-null?)
                              (string-split k #\space)))
                 (list (let ((v (string-trim-both v)))
                         (or (string->number v)
                             (match v
                               ("#t" #t)
                               ("#f" #f)
                               (v
                                (if (string-prefix? "'" v)
                                    (call-with-input-string (substring v 1) read)
                                    v)))))))
         #f))
      (_ #f)))
  (define (take-params exp)
    (reverse!
     (let lp ((params '()) (exp exp))
       (cond
        ((and (pair? exp) (parse-param (car exp)))
         => (lambda (param)
              (lp (cons param params) (cdr exp))))
        (else
         params)))))
  (define (node->presentation node level)
    (define (transform exp)
      (pre-post-order
       exp
       `((para . ,(lambda (tag . body)
                    (match body
                      ((('image ('@ . attrs)))
                       `(image (@ (inline? #f) . ,attrs)))
                      (_
                       `(p ,@body)))))
         (itemize . ,(lambda (tag attrs . body)
                       `(ul ,@body)))
         (item . ,(lambda (tag . body)
                    `(li ,@body)))
         (example . ,(lambda (tag . body)
                       `(pre ,@body)))
         (quotation . ,(lambda (tag . body)
                         `(blockquote ,@body)))
         (verbatim . ,(lambda (tag . body)
                        `(pre ,@body)))
         (smallexample . ,(lambda (tag . body)
                            `(pre (@ (text-height 24)) ,@body)))
         (iftex *preorder* . ,(lambda x *elided*))
         (title . ,(lambda x x))
         (subtitle . ,(lambda (tag . body)
                        `(p ,@body)))
         (author . ,(lambda (tag . body)
                      `(p ,@body)))
         (section . ,(lambda (tag . body)
                       (if (null? body)
                           *elided*
                           `(header ,@body))))
         (emph . ,(lambda (tag . body)
                    `(i ,@body)))
         (var . ,(lambda (tag . body)
                    `(i ,@body)))
         (code . ,(lambda (tag . body)
                    `(tt ,@body)))
         (email . ,(lambda (tag . body)
                      `(tt ,@body)))
         (strong . ,(lambda (tag . body)
                      `(b ,@body)))
         (result . ,(lambda (tag . body)
                      '(span (@ (font-family "Sans")) "⇒")))
         (c . ,(lambda _ *elided*))
         (uref . ,(lambda (tag attrs)
                    `(tt ,(cadr (assq 'url (cdr attrs))))))
         (ifnottex . ,(lambda (tag . body) body))
         (image . ,(lambda (tag attrs)
                     (let ((filename (cadr (assq 'file (cdr attrs))))
                           (width (and=> (assq 'width (cdr attrs)) cadr))
                           (height (and=> (assq 'height (cdr attrs)) cadr))
                           (extension (and=> (assq 'extension (cdr attrs)) cadr)))
                       `(image
                         (@ (image-filename
                             ,(if extension
                                  (string-append filename extension)
                                  filename))
                            . ,((lambda (tail)
                                  (if width
                                      `((image-width ,(string->number width))
                                        . ,tail)
                                      tail))
                                (if height
                                    `((image-height ,(string->number height)))
                                    '())))))))
         (titlepage . ,(lambda (tag . body) body))
         (% *preorder* . ,(lambda x x))
         (*text* . ,(lambda (tag text) text)))))
    (let* ((contents (cddr (node-ref node 'value))) ; cdr past texinfo and (% ..)
           (params (filter-map parse-param contents)))
      (decorate `(slide (@ (level ,level)
                           (title ,(node-ref node 'name)))
                        ,@(canonicalize (map transform contents)))
                params)))
  (with-seed
   (node-foldts
    (lambda (seed node)
      (with-seed seed (depth out)
                 (make-seed (1+ depth) out)))
    (lambda (seed kseed node)
      (with-seed seed (depth _)
                 (with-seed kseed (_ out)
                            (make-seed depth out))))
    (lambda (seed node)
      (with-seed seed (depth out)
                 (make-seed depth
                            (cons (node->presentation node depth)
                                  out))))
    '(0 ())
    (stexi->nodal-tree stexi #:initial-depth 1))
   (depth out)
   `(presentation
     (@ ,@(take-params (cddr stexi)))
     ,@(reverse out))))
