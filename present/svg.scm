;; guile-present
;; Copyright (C) 2007, 2009, 2010, 2012, 2014 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Routines to transform SXML documents from the presentation vocabulary
;; into SVG.
;;
;; The code in this file was originally presented in Andy Wingo's 2007
;; paper, @emph{Applications of fold to XML transformation}.
;;
;;; Code:

(define-module (present svg)
  :use-module (present util)
  :use-module (present fold)
  :export (presentation->svg))

;; Probably, more of this could be public.

(define (lookup params key)
  (cond ((null? params)
         (error "unbound param" key))
        ((assq key (car params)) => cadr)
        (else (lookup (cdr params) key))))

(define-syntax let-params
  (syntax-rules ()
    ((_ params (k ...) b b* ...)
     (let ((p params))
       (let ((k (lookup p 'k)) ...)
         b b* ...)))))

(define (make-layout x y)
  (cons x y))

(define (layout-x layout)
  (car layout))

(define (layout-y layout)
  (cdr layout))

(define (layout-advance layout dx dy)
  (make-layout (+ (layout-x layout) dx) (+ (layout-y layout) dy)))

(define (default-post tag params old-layout layout kids)
  (values
   layout
   (cons 'g kids)))

(define (make-text-x params layout)
  (layout-x layout))

(define (make-text-y params layout)
  (let-params params (text-height)
              (+ text-height (layout-y layout))))

(define (layout-advance-text-line params layout)
  (let-params params (text-height line-spacing)
    (layout-advance layout 0 (* text-height line-spacing))))

(define (specialize-param params k v)
  (cons (acons k v '()) params))

(define (make-tspan params layout text . line?)
  (let-params params (text-height)
     (match-bind
      "^( *)(.*)$" text (_ spaces text)
      (let ((indent (* (string-length spaces) 0.5 text-height)))
        `(tspan
          (@ ,@(if (or (null? line?) (car line?)) '((sodipodi:role "line")) '())
             (x ,(number->string (+ indent (make-text-x params layout))))
             (y ,(number->string (make-text-y params layout))))
          ,text)))))

(define (make-text params layout kids)
  (let-params params (text-height font-family line-spacing)
    `(text
      (@ (xml:space "preserve")
         (sodipodi:linespacing ,(number->string (* line-spacing 100)) "%")
         (font-size ,(number->string text-height))
         (font-family ,font-family)
         (x ,(number->string (make-text-x params layout)))
         (y ,(number->string (make-text-y params layout))))
      ,@kids)))

(define (text-handler text params layout) 
  (values
   (layout-advance-text-line params layout)
   (make-tspan params layout text)))

(define (p-post tag params old-layout layout kids)
  (values
   (let-params params (line-spacing para-spacing text-height)
     (layout-advance layout 0 (* para-spacing line-spacing text-height)))
   (make-text params old-layout kids)))

(define (ul-pre-layout tree params layout)
  (let-params params (margin-left)
    (layout-advance layout margin-left 0)))

(define (ul-post tag params old-layout layout kids)
  (values
   (make-layout (layout-x old-layout)
                (layout-y layout))
   `(g ,@kids)))

(define (make-bullet params layout)
  (make-text
   params layout
   (list (make-tspan params layout "*" #f))))

(define (li-post tag params old-layout layout kids)
  (let-params params (margin-left text-height line-spacing)
    (values
     layout
     `(g
       ,(make-bullet params (layout-advance old-layout
                                            (- margin-left) 0))
       ,@kids))))

(define (title-pre-layout tree params layout)
  (let-params params (margin-left margin-top)
    ;; go to the middle of the page?
    (layout-advance layout 0 (* 4 margin-top))))

(define (title-post tag params old-layout layout kids)
  (values
   layout
   (make-text params old-layout kids)))

(define (header-post tag params old-layout layout kids)
  (let-params params (margin-left margin-top)
    (values
     (layout-advance layout 0 margin-top)
     (make-text params old-layout kids))))

(define (slide-pre-layout tree params layout)
  (let-params params (margin-left margin-top)
    (make-layout margin-left margin-top)))

(define (slide-post tag params old-layout layout kids)
  (values
   old-layout
   (let-params params (title)
     `(g
       (@ (inkscape:groupmode "layer")
          (inkscape:label ,title))
       ,@kids))))

(define (presentation-post tag params old-layout layout kids)
  (values
   old-layout
   (let-params params (width height)
     `(svg
       (@ (xmlns "http://www.w3.org/2000/svg")
          (xmlns:sodipodi "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd")
          (xmlns:inkscape "http://www.inkscape.org/namespaces/inkscape")
          (width ,(number->string width))
          (height ,(number->string height))
          (version "1.0"))
       ,@kids))))

(define *presentation->svg-rules*
  `((presentation
     (post . ,presentation-post))
    (slide
     (pre-layout . ,slide-pre-layout)
     (post . ,slide-post))
    (title
     (pre-layout . ,title-pre-layout)
     (post . ,title-post))
    (header
     (post . ,header-post))
    (ul
     (pre-layout . ,ul-pre-layout)
     (post . ,ul-post))
    (li
     (post . ,li-post))
    (p
     (post . ,p-post))
    (*text* . ,text-handler)
    (*default*
     (post . ,default-post))))

(define *default-params*
  '((margin-left 64)
    (margin-right 64)
    (margin-top 64)
    (margin-bottom 64)
    (line-spacing 1.0)
    (para-spacing 0.5)
    (font-family "Serif")
    (text-height 56)
    (header-height 64)
    (width 1024)
    (height 768)))

(define *null-layout* #f)

(define *presentation-stylesheet*
  '((title (text-height 96))
    (header (text-height 64))
    (li (para-spacing 0.25))))

(define (presentation->svg presentation)
  "Convert an SXML document in the @code{presentation} vocabulary to a
multi-layered SVG.

The result will still be a document in SXML format, so if you want to
write it to disk, use @code{sxml->xml}. @xref{sxml simple,(sxml
simple),(sxml simple),guile-library,Guile Library}, for more
information.

The resulting SVG will be written with annotations readable by Inkscape,
a vector graphics editor, which help to make the SVG easily editable. If
your toolchain does not understand namespaces, you might want to filter
out elements that start with @samp{sodipodi:}, @samp{xmlns:}, and
@samp{inkscape:}.
"
  (define (layout)
    (fold-layout
     presentation
     *presentation->svg-rules*
     *default-params*
     *null-layout*
     *presentation-stylesheet*))
  (call-with-values layout
    (lambda (ret layout) ret)))
