;; guile-present
;; Copyright (C) 2007, 2014 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, see
;; <http://www.gnu.org/licenses/>.

(use-modules (unit-test)
             (oop goops)
             (present org-mode))

(define-class <test-org-mode> (<test-case>))

(define-method (test-minimal (self <test-org-mode>))
  (assert-equal '(presentation
                  (slide (@ (title "Foo") (level 1))
                         (header "Foo")))
                (call-with-input-string "* Foo" org->presentation)))

(define *top-srcdir* (or (getenv "top_srcdir") (car %load-path)))

(define-method (test-example-file (self <test-org-mode>))
  (assert-equal
   '(presentation
     (@ (org-params
         (OPTIONS
          . "H:4 num:nil toc:nil \\n:t @:t ::t |:t ^:t *:t TeX:t LaTeX:t")
         (TITLE . "Medios de (re)producción: GStreamer")))
     (slide (@ (title "TITLE") (level 0))
            (title "Medios"
                   "de"
                   "(re)producción:"
                   "GStreamer"))
     (slide (@ (title "Plan") (level 1))
            (header "Plan")
            (p "GStreamer: Multimedia para todos")
            (p "@fluendo.com: Qué es lo que"
               "estamos haciendo"))
     (slide (@ (title "GStreamer") (level 1))
            (header "GStreamer"))
     (slide (@ (title "¿Qué es GStreamer?") (level 2))
            (header "¿Qué es GStreamer?")
            (ul (li (p "Una librería multimedia y un"
                       "conjunto de plugins"))))
     (slide (@ (title "Breve ejemplo") (level 2))
            (header "Breve ejemplo")
            (p "v4l2src ! ffmpegcolorspace !" "ximagesink"))
     (slide (@ (title "GStreamer: Historia") (level 2))
            (header "GStreamer: Historia")
            (p "0.0.9 Octubre 1999"
               "0.1.0 Enero 2001"
               "0.3.0 Diciembre 2001"
               "0.4.0 Julio 2002"
               "0.6.0 Febrero 2003: API estable"
               "0.8.0 Marzo 2004"
               "0.10.0 Diciembre 2005"))
     (slide (@ (title "GStreamer: Historia") (level 2))
            (header "GStreamer: Historia")
            (p "No nos pueden acusar de aumentar"
               "innecesariamente la versión ;)")
            (p "Próximo cambio incompatible será"
               "el último: 1.0"))
     (slide (@ (title "Una visita guiada") (level 2))
            (header "Una visita guiada")
            (p "GStreamer, visto por sus usuarios"))
     (slide (@ (title "Reproductores") (level 2))
            (header "Reproductores")
            (p "Históricamente las primeras")
            (p "Sorprendemente difícil"))
     (slide (@ (title "Reproductores: soluciones") (level 2))
            (header "Reproductores: soluciones")
            (p "Interfaz")
            (ul (li (p "API genérico"))
                (li (p "Procesamiento ocurre en hilos"
                       "de ejecución (threads)"))
                (li (p "Los pipeline (tuberías de"
                       "media) informan a la"
                       "aplicación con mensajes"))))
     (slide (@ (title "Reproductores: soluciones") (level 2))
            (header "Reproductores: soluciones")
            (p "Sincronización entre audio y" "vídeo")
            (ul (li (p "Usar hilos soluciona este" "problema"))))
     (slide (@ (title "Reproductores") (level 2))
            (header "Reproductores")
            (p "Autoenchufamiento: tú lo pones,"
               "GStreamer lo reproduce")
            (ul (li (p "Registro de cazatipos" "(typefinders)"))
                (li (p "Registro de capacidades de" "los plugin"))
                (li (p "Elementos que automatizan el"
                       "proceso de autoenchufamiento"))))
     (slide (@ (title "Transcodificación / grabación")
               (level 2))
            (header "Transcodificación / grabación")
            (p "E.g. thoggen conversor de DVD,"
               "aplicaciones de producción de"
               "radio, flumotion"))
     (slide (@ (title "(Trans)codificación: soluciones")
               (level 2))
            (header "(Trans)codificación: soluciones")
            (p "La robustez")
            (ul (li (p "Tuberías de muy larga" "duración"))
                (li (p "Tuberías muy cargadas"))
                (li (p "Gran variedad de códigos"))))
     (slide (@ (title "(Trans)codificación: soluciones")
               (level 2))
            (header "(Trans)codificación: soluciones")
            (p "Necesidades")
            (ul (li (p "\"Bindings\" para muchos"
                       "lenguajes de programación"))
                (li (p "Muchas fuentes de grabación")
                    (ul (li (p "v4l, firewire, ..."))))
                (li (p "Flujo de dados por TCP"))
                (li (p "Sincronización de relojes por" "red"))))
     (slide (@ (title "Aplicaciones empotradas") (level 2))
            (header "Aplicaciones empotradas")
            (p "GStreamer es cada vez más común"
               "en los dispositivos empotrados"
               "(\"embedded\"): Maemo / GMAE / ...")
            (ul (li (p "Librería de peso ligero"))
                (li (p "Escalar hacia abajo"))
                (li (p "Usar hardware especializado"
                       "para el procesamiento"))))
     (slide (@ (title "Editores no linear") (level 2))
            (header "Editores no linear")
            (p "El editor de vídeo PiTiVi, el"
               "editor de audio Jokosher")
            (ul (li (p "\"Búsquedas de segmento\":"
                       "configurar el pipeline a"
                       "reproducir entre los segundos"
                       "3 y 10, por ejemplo"))
                (li (p "Librería de edición no" "linear: GNonLin"))))
     (slide (@ (title "En plan teleconferencia") (level 2))
            (header "En plan teleconferencia")
            (p "Telepathy, muchas aplicaciones" "empotradas")
            (ul (li (p "Latencia dinámica"))
                (li (p "Sincronización entre relojes"))
                (li (p "Protocolos rtp/rtsp"))
                (li (p "Algoritmos amortiguadores"))))
     (slide (@ (title "Producción de media") (level 2))
            (header "Producción de media")
            (p "La dimensión desconocida:"
               "aplicaciones VJ/DJ,"
               "sintetizadores")
            (ul (li (p "características compatibles"
                       "con procesar en tiempo real"))
                (li (p "eficiencia"))))
     (slide (@ (title "@fluendo.com: Flumotion") (level 1))
            (header "@fluendo.com: Flumotion"))
     (slide (@ (title "¿Qué es Flumotion?") (level 2))
            (header "¿Qué es Flumotion?")
            (p "Un servidor de difusión de" "media")
            (p "Una solución integrada:"
               "grabación, efectos,"
               "codificación, difusión"))
     (slide (@ (title "Demonstración") (level 2))
            (header "Demonstración")
            (p "(difundir de webcam)"))
     (slide (@ (title "Historia") (level 2))
            (header "Historia")
            (ul (li (p "Flumotion difunde GUADEC:" "Junio 2004"))
                (li (p "0.0.1 Septiembre 2004"))
                (li (p "0.1.0 Octubre 2004"))
                (li (p "0.2.0 Marzo 2006"))
                (li (p "0.4.0 Enero 2007")))
            (p "¿Miedo del 1.0? :)"))
     (slide (@ (title "Estatus") (level 2))
            (header "Estatus")
            (p "Difundió el evento GPLv3")
            (p "El la base de la plataforma de"
               "streaming que tiene Fluendo")
            (ul (li (p "Difusión en vivo y"
                       "\"on-demand\" de muchos grupos"
                       "mediáticos españoles")))
            (p "Todavía evolucionando"))
     (slide (@ (title "@fluendo.com: Otros") (level 2))
            (header "@fluendo.com: Otros")
            (p "Centro de media Elisa")
            (p "Editor de vídeo PiTiVi"))
     (slide (@ (title "Conclusión") (level 2))
            (header "Conclusión")
            (p "¿Preguntas?")
            (p "¡Queremos buena gente!")
            (p "Andy Wingo, JAH"
               "(Just another hacker)"
               "wingo@fluendo.com")
            (p "Julien Moutte, CEO" "julien@fluendo.com")))
   (call-with-input-file
       (in-vicinity *top-srcdir* "tests/jpl-outline-es")
     org->presentation)))

(setlocale LC_ALL "")
(exit-with-summary (run-all-defined-test-cases))
