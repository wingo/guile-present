;;; guix potluck package
;;; Copyright (C) 2017 Andy Wingo

;;; This file is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.  No warranty.  See
;;; https://www.gnu.org/licenses/gpl.html for a copy of the GPLv3.

(potluck-package
  (name "guile-present")
  (version "v0.3.0-14-g3b0d2d9")
  (source
    (potluck-source
      (git-uri "https://gitlab.com/wingo/guile-present")
      (git-commit "3b0d2d967909dabdd3f86d3a50da79c6b1a8aeb2")
      (sha256 "0sr5snj921k91frh5dx1gsqrc3679liydv3b6nwxkl48m7h5mals")))
  (build-system 'gnu)
  (inputs '("guile@2.0" "guile-cairo" "guile-lib"))
  (native-inputs '("autoconf" "automake" "libtool" "pkg-config" "texinfo"))
  (propagated-inputs '())
  (arguments
    '(#:phases
      (modify-phases
        %standard-phases
        (add-before
          'configure
          'autoconf
          (lambda _ (zero? (system* "autoreconf" "-vfi")))))))
  (home-page "https://gitlab.com/wingo/guile-present")
  (synopsis "Presentation software that can generate PDF slides from
Texinfo.")
  (description "Guile-Present is a library for slide rendering.  It
uses Guile's Texinfo parser to parse presentations written in that
language, and renders them to PDF using Guile-Cairo.")
  (license 'lgpl3+))
