This directory defines potluck packages.  Each file in this directory should
define one package.  See https://guix-potluck.org/ for more information.
